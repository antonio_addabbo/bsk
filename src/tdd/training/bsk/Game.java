package tdd.training.bsk;

import java.util.ArrayList;

public class Game {

	/**
	 * It initializes an empty bowling game.
	 */
	
	private ArrayList<Frame> frames;
	private int firstBonusThrow;
	private int secondBonusThrow;
	
	
	
	public Game() {
		this.frames = new ArrayList<Frame>(10);
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		if(this.frames.size() <10 )
			this.frames.add(frame);	
			else 
				throw new BowlingException();
	
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return this.frames.get(index);
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int score = 0;
		
		for(int i = 0; i < this.frames.size(); i++) {
			if(this.frames.get(i).isSpare()) {
				if(i < 9)
					this.frames.get(i).setBonus(this.frames.get(i+1).getFirstThrow());
				else
					this.frames.get(i).setBonus(this.firstBonusThrow);
			}
			
			if(i < 9 && this.frames.get(i).isStrike() && !this.frames.get(i+1).isStrike()) {
				this.frames.get(i).setBonus(this.frames.get(i+1).getFirstThrow() + this.frames.get(i+1).getSecondThrow());
			}
			
			if(this.frames.get(i).isStrike()) {
				if(i < 8 && this.frames.get(i+1).isStrike())
					this.frames.get(i).setBonus(this.frames.get(i+1).getFirstThrow() + this.frames.get(i+2).getFirstThrow());
				if(i == 8 && this.frames.get(i+1).isStrike())
					this.frames.get(i).setBonus(this.frames.get(i+1).getFirstThrow() + this.firstBonusThrow);
				if(i == 9)
					this.frames.get(i).setBonus(this.firstBonusThrow + this.secondBonusThrow);
			}	
			score += this.frames.get(i).getScore();
		}
		
		return score;
	}
}
