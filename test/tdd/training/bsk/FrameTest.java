package tdd.training.bsk;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;


public class FrameTest {

	@Test
	//Test UserStory1
	public void testGetThrow() throws BowlingException{
		int firstThrow = 3;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(firstThrow,frame.getFirstThrow());
		assertEquals(secondThrow, frame.getSecondThrow());
	}
	
	@Test
	//Test UserStory2
	public void testGetFrameScore() throws BowlingException{
		int firstThrow = 2;
		int secondThrow = 5;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(firstThrow + secondThrow,frame.getScore());
	}
	
	@Test
	//Test UserStory3
	public void testGetFrameAtIndex() throws BowlingException{
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (1,5));
		frames.add(new Frame (2,5));
		frames.add(new Frame (1,1));
		frames.add(new Frame (4,2));
		frames.add(new Frame (8,0));
		frames.add(new Frame (2,3));
		frames.add(new Frame (1,3));
		frames.add(new Frame (1,6));
		frames.add(new Frame (2,0));
		frames.add(new Frame (10,0));
		
		for(int i = 0;i<10;i++)
			game.addFrame(frames.get(i));
		
		for(int j = 0;j<10;j++)
			assertEquals(true,frames.get(j).equals(game.getFrameAt(j)));
	}
	
	@Test
	//Test UserStory4
	public void testGetGameScore() throws BowlingException{
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (1,5));
		frames.add(new Frame (2,5));
		frames.add(new Frame (1,1));
		frames.add(new Frame (4,2));
		frames.add(new Frame (8,0));
		frames.add(new Frame (2,3));
		frames.add(new Frame (1,3));
		frames.add(new Frame (1,6));
		frames.add(new Frame (2,0));
		frames.add(new Frame (10,0));
		
		for(int i = 0;i<10;i++)
			game.addFrame(frames.get(i));
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		assertEquals(score, game.calculateScore());
	}
	
	@Test
	//Test UserStory5
	public void testGetGameWithSpare() throws BowlingException{
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (1,9));
		frames.add(new Frame (3,6));
		frames.add(new Frame (7,2));
		frames.add(new Frame (3,6));
		frames.add(new Frame (4,4));
		frames.add(new Frame (5,3));
		frames.add(new Frame (3,3));
		frames.add(new Frame (4,5));
		frames.add(new Frame (8,1));
		frames.add(new Frame (2,6));
		
		for(int i = 0;i<10;i++)
			game.addFrame(frames.get(i));
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
				
		assertEquals(score +3, game.calculateScore());
	}
	
	@Test
	//Test UserStory6
	public void testGetGameWithStrike() throws BowlingException{
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (10,0));
		frames.add(new Frame (3,6));
		frames.add(new Frame (7,2));
		frames.add(new Frame (3,6));
		frames.add(new Frame (4,4));
		frames.add(new Frame (5,3));
		frames.add(new Frame (3,3));
		frames.add(new Frame (4,5));
		frames.add(new Frame (8,1));
		frames.add(new Frame (2,6));
		
		for(int i = 0;i<10;i++)
			game.addFrame(frames.get(i));
				
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	//Test UserStory7
	public void testGetGameWithStrikeAndSpare() throws BowlingException{
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (10,0));
		frames.add(new Frame (4,6));
		frames.add(new Frame (7,2));
		frames.add(new Frame (3,6));
		frames.add(new Frame (4,4));
		frames.add(new Frame (5,3));
		frames.add(new Frame (3,3));
		frames.add(new Frame (4,5));
		frames.add(new Frame (8,1));
		frames.add(new Frame (2,6));
		
		for(int i = 0;i<10;i++)
			game.addFrame(frames.get(i));
		
		assertEquals(103, game.calculateScore());
	}
	
	
	@Test
	//Test UserStory8
	public void testGetGameWithManyStrike() throws BowlingException{
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (7,2));
		frames.add(new Frame (3,6));
		frames.add(new Frame (4,4));
		frames.add(new Frame (5,3));
		frames.add(new Frame (3,3));
		frames.add(new Frame (4,5));
		frames.add(new Frame (8,1));
		frames.add(new Frame (2,6));
		
		for(int i = 0;i<10;i++)
			game.addFrame(frames.get(i));
		
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	//Test UserStory9
	public void testGetGameWithManySpares() throws BowlingException{
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (8,2));
		frames.add(new Frame (5,5));
		frames.add(new Frame (7,2));
		frames.add(new Frame (3,6));
		frames.add(new Frame (4,4));
		frames.add(new Frame (5,3));
		frames.add(new Frame (3,3));
		frames.add(new Frame (4,5));
		frames.add(new Frame (8,1));
		frames.add(new Frame (2,6));
		
		for(int i = 0;i<10;i++)
			game.addFrame(frames.get(i));
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		assertEquals(score +5+7, game.calculateScore());
	}
	
	@Test
	//Test UserStory10
	public void testGetGameWithLastSpare() throws BowlingException{
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (1,5));
		frames.add(new Frame (3,6));
		frames.add(new Frame (7,2));
		frames.add(new Frame (3,6));
		frames.add(new Frame (4,4));
		frames.add(new Frame (5,3));
		frames.add(new Frame (3,3));
		frames.add(new Frame (4,5));
		frames.add(new Frame (8,1));
		frames.add(new Frame (2,8));
		
		for(int i = 0;i<10;i++)
			game.addFrame(frames.get(i));
		
		game.setFirstBonusThrow(7);
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		assertEquals(score + game.getFirstBonusThrow(), game.calculateScore());
	}
	
	@Test
	//Test UserStory11
	public void testGetGameWithLastStrike() throws BowlingException{
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (1,5));
		frames.add(new Frame (3,6));
		frames.add(new Frame (7,2));
		frames.add(new Frame (3,6));
		frames.add(new Frame (4,4));
		frames.add(new Frame (5,3));
		frames.add(new Frame (3,3));
		frames.add(new Frame (4,5));
		frames.add(new Frame (8,1));
		frames.add(new Frame (10,0));
		
		for(int i = 0;i<10;i++)
			game.addFrame(frames.get(i));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		int score = 0;
		
		for(int j = 0; j < frames.size(); j++)
			score += (frames.get(j).getFirstThrow() + frames.get(j).getSecondThrow());
		
		
		assertEquals(score+game.getFirstBonusThrow()+game.getSecondBonusThrow(), game.calculateScore());
	}

	@Test
	//Test UserStory12
	public void testGetPerfectGame() throws BowlingException{
		Game game = new Game();
		ArrayList<Frame> frames = new ArrayList<Frame>(10);
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		frames.add(new Frame (10,0));
		
		for(int i = 0;i<10;i++)
			game.addFrame(frames.get(i));
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}
	
	

}
